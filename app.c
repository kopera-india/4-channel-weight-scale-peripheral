/***************************************************************************//**
 * @file
 * @brief Core application logic.
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * SPDX-License-Identifier: Zlib
 *
 * The licensor of this software is Silicon Laboratories Inc.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 ******************************************************************************/
#include "em_common.h"
#include "app_assert.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#include "app.h"
#include "app_log.h"
#include "app_assert.h"
#include "stdbool.h"

#include "sl_simple_button_instances.h"


// The advertising set handle allocated from Bluetooth stack.
static uint8_t advertising_set_handle = 0xff;

#define BONDING_WITHOUT_MITM_PROTECTION (0)
#define ALLOW_BONDING (1)
#define DONOT_ALLOW_BONDING (1)
#define SYSTEM_BOOT_MODE_NORMAL (0x0)
#define CONNECTION_HANDLE_INVALID     ((uint8_t)0xFFu)


// If the notification is enabled or not
static int16_t connection = CONNECTION_HANDLE_INVALID;
static uint8_t bonding_handle = SL_BT_INVALID_BONDING_HANDLE;

#define ONE_SEC_TIMER_TICKS 32768
#define RESET_TIMER_HANDLE 1
#define SINGLE_SHOT 1

void sl_button_on_change(const sl_button_t *handle)
{
  app_log_info("button status changed.\n\r");
  if (&sl_button_btn0 == handle) {

    if (sl_button_get_state(handle) == SL_SIMPLE_BUTTON_PRESSED) {
        app_log_info("button status pressed.\n\r");
        sl_bt_system_set_soft_timer(5*ONE_SEC_TIMER_TICKS, RESET_TIMER_HANDLE, SINGLE_SHOT);
    }
    else if (sl_button_get_state(handle) == SL_SIMPLE_BUTTON_RELEASED) {
        sl_bt_system_set_soft_timer(0, RESET_TIMER_HANDLE, SINGLE_SHOT);
        app_log_info("button status released.\n\r");
    }
    else if (sl_button_get_state(handle) == SL_SIMPLE_BUTTON_DISABLED) {
        app_log_info("button status disabled.\n\r");
    }
  }

}

/**************************************************************************//**
 * Application Init.
 *****************************************************************************/
SL_WEAK void app_init(void)
{
  sl_simple_button_init_instances();
  app_log_info("weight channel sensor initialised\n");

  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application init code here!                         //
  // This is called once during start-up.                                    //
  /////////////////////////////////////////////////////////////////////////////
}

/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
}

typedef enum {
  CheckIfBonded,
  BondingFailed,
  Reset,

}PeripheralWorkFlow_Typedef;

PeripheralWorkFlow_Typedef pwf;
bool isBonded = false;

/**************************************************************************//**
 * Bluetooth stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void sl_bt_on_event(sl_bt_msg_t *evt)
{
  sl_status_t sc;
  bd_addr address;
  uint8_t address_type;
  uint8_t system_id[8];

  switch (SL_BT_MSG_ID(evt->header)) {

    // -------------------------------
    // This event indicates the device has started and the radio is ready.
    // Do not call any stack command before receiving this boot event!
    case sl_bt_evt_system_boot_id:
      // Print boot message.
      app_log_debug("Bluetooth stack booted: v%d.%d.%d-b%d\n",
                   evt->data.evt_system_boot.major,
                   evt->data.evt_system_boot.minor,
                   evt->data.evt_system_boot.patch,
                   evt->data.evt_system_boot.build);

      // Extract unique ID from BT Address.
      sc = sl_bt_system_get_identity_address(&address, &address_type);
      app_assert_status(sc);

      // Pad and reverse unique ID to get System ID.
      system_id[0] = address.addr[5];
      system_id[1] = address.addr[4];
      system_id[2] = address.addr[3];
      system_id[3] = 0xFF;
      system_id[4] = 0xFE;
      system_id[5] = address.addr[2];
      system_id[6] = address.addr[1];
      system_id[7] = address.addr[0];

      sc = sl_bt_gatt_server_write_attribute_value(gattdb_system_id,
                                                   0,
                                                   sizeof(system_id),
                                                   system_id);
      app_assert_status(sc);

      app_log_debug("Bluetooth %s address: %02X:%02X:%02X:%02X:%02X:%02X\n",
                   address_type ? "static random" : "public device",
                   address.addr[5],
                   address.addr[4],
                   address.addr[3],
                   address.addr[2],
                   address.addr[1],
                   address.addr[0]);

      sc = sl_bt_sm_configure(BONDING_WITHOUT_MITM_PROTECTION, sl_bt_sm_io_capability_noinputnooutput);
      app_assert_status(sc);

//      sc = sl_bt_sm_set_bondable_mode(ALLOW_BONDING);
//      app_assert_status(sc);

      pwf = CheckIfBonded;
      sl_bt_sm_list_all_bondings();

      break;

    case  sl_bt_evt_sm_list_bonding_entry_id:

      if (pwf == CheckIfBonded) {
          isBonded = true;
      }
#if 0
      else if (pwf == Reset) {
        app_log_debug("[peripheral]Bonding Enrty:-\n");
        app_log_debug("[peripheral]Bonding Handle %d\n", evt->data.evt_sm_list_bonding_entry.bonding);
        app_log_debug("[peripheral]BT ADDR:-");

        bd_addr address = evt->data.evt_sm_list_bonding_entry.address;

        app_log_debug("Bluetooth %s address: %02X:%02X:%02X:%02X:%02X:%02X\n",
                     evt->data.evt_sm_list_bonding_entry.address_type ? "static random" : "public device",
                     address.addr[5],
                     address.addr[4],
                     address.addr[3],
                     address.addr[2],
                     address.addr[1],
                     address.addr[0]);

        app_log_append("\n\r");
        sc = sl_bt_sm_delete_bonding(evt->data.evt_sm_list_bonding_entry.bonding);
        app_assert_status(sc);

      }
#endif
      break;

    case  sl_bt_evt_sm_list_all_bondings_complete_id:

      if (pwf == CheckIfBonded) {
          if (isBonded == true) {
              // if already bonded donot allow bonding.
              sc = sl_bt_sm_set_bondable_mode(DONOT_ALLOW_BONDING);
              app_assert_status(sc);

          } else {
              sc = sl_bt_sm_set_bondable_mode(ALLOW_BONDING);
              app_assert_status(sc);
          }
          // Create an advertising set.
          sc = sl_bt_advertiser_create_set(&advertising_set_handle);
          app_assert_status(sc);


          // TODO :- donot advertise weight service if bonded.

          // TODO :- check adv tx power setting.
          //sl_bt_system_set_max_tx_power(0, 0);

          // Set advertisements to all 3 channels
          sl_bt_advertiser_set_channel_map(0, 7);

          // Set advertising interval to 100ms.
          sc = sl_bt_advertiser_set_timing(
            advertising_set_handle,
            1600, // min. adv. interval (milliseconds * 1.6)
            1600, // max. adv. interval (milliseconds * 1.6)
            0,   // adv. duration
            0);  // max. num. adv. events
          app_assert_status(sc);

          // Start general advertising and enable connections.
          sc = sl_bt_advertiser_start(
            advertising_set_handle,
            advertiser_general_discoverable,
            advertiser_connectable_non_scannable);

          app_assert_status(sc);

          app_log_info("[peripheral] Advertising started in Non Discoverable.");
          app_log_append("\n\r");
      }

      break;


    // -------------------------------
    // This event indicates that a new connection was opened.
    case sl_bt_evt_connection_opened_id:

      app_log_debug("[peripheral] New connection opened: connection handle 0x%x  bonding handle 0x%x", evt->data.evt_connection_opened.connection,
                    evt->data.evt_connection_opened.bonding);
      app_log_append("\n\r");

      // save the connection handle.
      connection = evt->data.evt_connection_opened.connection;
      bonding_handle = evt->data.evt_connection_opened.bonding;

      // Request a change in the connection parameters of a Bluetooth connection
      // 200ms connection interval, latency of 4 intervals
      sl_bt_connection_set_parameters(evt->data.evt_connection_opened.connection, 160, 160, 5, 450, 0, 0xffff);

      // start a timer to read from adc at much faster interval and can take
      // a average.
      break;

    // event is triggered any time the connection parameters are changed and any time
    // a connection is established.
    case sl_bt_evt_connection_parameters_id:
      app_log_debug("[peripheral]Connection params changed security mode");
      app_log_append("\n\r");

      if (evt->data.evt_connection_parameters.security_mode == sl_bt_connection_mode1_level2) {
        app_log_debug("Security increased to level 2.");
        app_log_append("\n\r");
      }
    break;

    case sl_bt_evt_sm_bonded_id:
      if (evt->data.evt_sm_bonded.bonding != SL_BT_INVALID_BONDING_HANDLE)
      {
          app_log_info("[peripheral] Device is bonded.");
          app_log_append("\n\r");

          bonding_handle = evt->data.evt_sm_bonded.bonding;
          //TODO disalow gecko_cmd_sm_set_bondable_mode(0)
          sc = sl_bt_sm_set_bondable_mode(DONOT_ALLOW_BONDING);
          app_assert_status(sc);


      } else {
          app_log_error("[peripheral] Bonding handle invalid.");
          app_log_append("\n\r");
          app_log_info("[peripheral]Deleting all bonding.");
          app_log_append("\n\r");
          sc = sl_bt_sm_delete_bondings();
          app_assert_status(sc);

      }
    break;

    case sl_bt_evt_sm_bonding_failed_id:
      app_log_debug("[peripheral]Bonding with device failed reason %d.", evt->data.evt_sm_bonding_failed.reason);
      app_log_debug("[peripheral]Deleting all bonding.");
      app_log_append("\n\r");
      sc = sl_bt_sm_delete_bondings();
      app_assert_status(sc);
      bonding_handle = SL_BT_INVALID_BONDING_HANDLE;
      sc = sl_bt_sm_set_bondable_mode(ALLOW_BONDING);
      app_assert_status(sc);

      //sl_bt_system_reset(SYSTEM_BOOT_MODE_NORMAL);
      break;
    // -------------------------------
    // This event indicates that a connection was closed.
    case sl_bt_evt_connection_closed_id:

      app_log_debug("[peripheral]Connection closed.");
      app_log_append("\n\r");

      connection = CONNECTION_HANDLE_INVALID;

      // we need to start adv again.
      // Restart advertising after client has disconnected.
      if (isBonded == true) {
          // TODO remove weight service from adv this will prevent others from
          // connecting.
      }

      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_non_scannable);
      app_assert_status(sc);

      break;

    ///////////////////////////////////////////////////////////////////////////
    // Add additional event handlers here as your application requires!      //
    ///////////////////////////////////////////////////////////////////////////
    case  sl_bt_evt_gatt_server_user_read_request_id:
      app_log_debug("read request characteristic handle 0x%x operation 0x%x gattdb_weight_channel_data 0x%x",
                    evt->data.evt_gatt_server_user_read_request.characteristic,
                    evt->data.evt_gatt_server_user_read_request.att_opcode,
                    gattdb_weight_channel_data);
      app_log_append("\r\n");
      // if the characteristic handle matches weight characteristics handle and
      if ((evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_weight_channel_data) &&
         (evt->data.evt_gatt_server_user_read_request.att_opcode == sl_bt_gatt_read_request)) {
          app_log_debug("Sending read response.\n\r");
          uint8_t   att_errorcode = 0; // No-error
          size_t  value_len = 4;
          uint32_t tmp_value = 0xDEADBEEF;
          uint16_t sent_len;

          sc = sl_bt_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
                                                         evt->data.evt_gatt_server_user_read_request.characteristic,
                                                         att_errorcode,
                                                         value_len,
                                                         (const uint8_t*)&tmp_value,
                                                         &sent_len
                                                         );
          app_assert_status(sc);

          app_log_debug("Sent items %d", sent_len);
          app_log_debug("\r\n");

      }

    break;

    case sl_bt_evt_system_soft_timer_id:

      if(evt->data.evt_system_soft_timer.handle == RESET_TIMER_HANDLE)
      {
          app_log_debug("Timer timeout handle %d", RESET_TIMER_HANDLE);
          app_log_debug("Resetting");
          app_log_debug("\r\n");
          //delete all bondings
          sc = sl_bt_sm_delete_bondings();
          app_assert_status(sc);

          //reset system
          sl_bt_system_reset(SYSTEM_BOOT_MODE_NORMAL);
      }

      break;

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}
